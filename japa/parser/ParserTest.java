package se701.assignment4.yeya;

import static org.junit.Assert.fail;
import japa.parser.JavaParser;
import japa.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParserTest {

	private static final String PROJECT_DIR = "/Users/michaellittle/Repos/se701Assignment4_tests";
	private static final String CORRECT_DIR = PROJECT_DIR + "/correct";
	private static final String INCORRECT_DIR = PROJECT_DIR + "/incorrect";

	@Parameters(name= "{index} file: {0}")
	public static Collection<Object[]> files() {

		Collection<File> files = new ArrayList<File>();

		//files.addAll(Arrays.asList(new File(CORRECT_DIR).listFiles()));
		files.addAll(Arrays.asList(new File(INCORRECT_DIR).listFiles()));

		Collection<Object[]> params = new ArrayList<Object[]>();
 
		for (File f : files) {
			params.add(new File[] { f });
		}

		return params;
	}

	private void compileFile(File file) throws ParseException, IOException {
		MainRunner.visit(JavaParser.parse(file));
	}

	private File file;

	public ParserTest(File file) {
		this.file = file;
	}

	@Test
	public void compileParser() {
		try {
			compileFile(this.file);
			if (this.file.getParent().equals(INCORRECT_DIR)) {
				fail("\"" + file.getName() + "\" compiled, should have failed.");
			}
		} catch (ParseException | IOException | CompilationException e) {
			if (this.file.getParent().equals(CORRECT_DIR)) {
				fail(file.getName()
						+ " failed to parse, should have compiled:\n"
						+ e.getMessage());
			}
		}
		
		
	}
}
